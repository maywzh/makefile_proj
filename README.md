## 通用makefile模版

编写makefile是一项重复性很高的工作，因为很多项目结构都是类似的。

编写一个通用的模版，在写c/c++项目的时候只需要改动少量配置即可。

```makefile
#-------------------------配置区域-----------------------
 
#DIR指的是.o文件和.c文件所在的目录
DIR=
 
#BIN指的是最终生成的目标对象名(包括路径)，它可以是可执行程序、动态链接库或静态链接库
BIN=$(DIR)/
 
#SHARE特指链接生成动态链接库对象时的编译选项
SHARE=--share
 
#CFLAG即compile flag，表示在编译时所加入的选项参数
#参数包括  
#-Wall  : 编译后显示所有警告信息
#-g     : 编译时加入调试信息，以便之后可以用gdb调试
#-fPIC  : 编译动态链接库时加入的选项
#-I./inc: -I选项指定从哪个目录寻找头文件，在这指定之后，在源文件中包含头文件就可以使用<>，这里./inc即为指定的目录
CFLAG=
 
#LFLAG即library flag，表示链接生成可执行程序时所要链接的所有库的选项参数
#-L./lib : -L指示动态/静态链接库所在的目录，这里./lib即所在的目录
#-l      : -l指示动态/静态链接库的名字，注意: 这里的库名字并不包括前缀(lib)和后缀(.a或.so)
#$(SHARE) : 取SHARE变量对应的动态链接库链接选项--share
LFLAG=
 
#CC即编译链接命令gcc 用于编译或者链接动态库以及可执行程序
CC=gcc
 
#AR即ar -cr ，ar -cr是专门链接生成静态库的命令
#-c : 即create，创建静态库
#-r : 即replace，当静态库改变时，替代原有静态库
AR=ar -cr
 
#gcc -o 指定输出文件
CO=$(CC) -o
 
 
#-------------------------以下为通用不变区域-----------------------
 
#SRC指的是指定目录下的所有.c文件名，OBJ指的是指定目录下的所有.o文件名
SRC=$(wildcard $(DIR)/*.c)
OBJ=$(patsubst %.c, %.o, $(SRC))

#链接命令
all:$(BIN)
$(BIN):$(OBJ)
	$(CO) $@ $^ $(LFLAG)
 
#编译命令
$(DIR)/%.o:$(DIR)/%.c
	$(CC) $@ -c $< $(CFLAG)
 
 
#清除无用文件
.PHONY:clean
clean:
	rm $(OBJ) $(BIN)

```

一个合理的编译工作流是在针对每个子目录编写合适的makefile，最后在项目根目录下编写联合编译makefile，实现一键编译功能



## 为子目录编写makefile

一般的项目结构

- bin目录：一般存放可执行程序
- etc目录：存放配置文件
- lib目录 ：存放所有的库文件，包括动态库和静态库
- inc目录：存放所有的公共头文件，一般是动态库和静态库对应的头文件
- src目录：存放项目源码文件和编译它的Makefile文件
- static目录：存放静态库源文件和编译它的Makefile文件
- dynamic目录：存放动态库源文件和编译它的Makefile文件

### static目录 
为static目录，即静态链接库源代码目录。在此目录下要生成静态链接库文件libstatic_test.a



Makefile文件：
```makefile
#------------------------------配置区域----------------------------
 
DIR=./static
 
#静态库名以lib为前缀，.a为后缀
BIN=$(DIR)/libstatic_test.a
 
SHARE=--share
 
CFLAG=
 
LFLAG=
 
CC=gcc -o
 
AR=ar -cr
 
#编译静态库用ar -cr命令
CO=$(AR)
#-------------------------以下为通用不变区域-----------------------
 
SRC=$(wildcard $(DIR)/*.c)
OBJ=$(patsubst %.c, %.o, $(SRC))
 
all:$(BIN)
$(BIN):$(OBJ)
	$(CO) $@ $^ $(LFLAG)
 
$(DIR)/%.o:$(DIR)/%.c
	$(CC) $@ -c $< $(CFLAG)
 
.PHONY:clean
clean:
	rm $(OBJ) $(BIN)
```

### dynamic目录
为dynamic目录，即动态链接库源代码目录，在此目录下生成动态链接库 libdynamic_test.so



Makefile文件：
```makefile
#------------------------------配置区域----------------------------
 
DIR=./dynamic
 
#动态库名以lib为前缀，以.so为后缀
BIN=$(DIR)/libdynamic_test.so
 
SHARE=--share
 
#动态链接库编译时需要-fPIC选项
CFLAG=-fPIC
 
#链接动态库需要--share选项
LFLAG=$(SHARE)
 
CC=gcc -o
 
AR=ar -cr
 
#编译动态链接库用gcc -o命令
CO=$(CC)
#-------------------------以下为通用不变区域-----------------------
 
SRC=$(wildcard $(DIR)/*.c)
OBJ=$(patsubst %.c, %.o, $(SRC))
 
all:$(BIN)
$(BIN):$(OBJ)
	$(CO) $@ $^ $(LFLAG)
 
$(DIR)/%.o:$(DIR)/%.c
	$(CC) $@ -c $< $(CFLAG)
 
.PHONY:clean
clean:
	rm $(OBJ) $(BIN)
```

### src目录
为src目录，即工程源代码目录。在此目录下要通过链接动态链接库libdynamic_test.so和静态库libstatic_test.a 来生成可执行文件main



Makefile文件：
```makefile
#-------------------------配置区域-----------------------
 
#源码所在的目录
DIR=./src
 
#在源码所在目录下生成可执行文件main
BIN=$(DIR)/main
 
SHARE=--share
 
#显示所有编译警告，并指示公共头文件(静态库和动态库的头文件)所在目录
CFLAG= -Wall -I../inc
 
#在../lib目录中链接两个外部库文件
LFLAG=-L../lib -ldynamic_test -lstatic_test
 
CC=gcc -o
 
AR=ar -cr
 
#使用gcc -o 链接命令
CO=$(CC)
 
 
#-------------------------以下为通用不变区域-----------------------
 
#SRC指的是指定目录下的所有.c文件名，OBJ指的是指定目录下的所有.o文件名
SRC=$(wildcard $(DIR)/*.c)
OBJ=$(patsubst %.c, %.o, $(SRC))
 
#链接命令
all:$(BIN)
$(BIN):$(OBJ)
	$(CO) $@ $^ $(LFLAG)
 
#编译命令
$(DIR)/%.o:$(DIR)/%.c
	$(CC) $@ -c $< $(CFLAG)
 
 
#清除无用文件
.PHONY:clean
clean:
	rm $(OBJ) $(BIN)
```
以上三步完成后，整个工程的目录树是这样的：

![Screen Shot 2018-10-27 at 2.37.29 PM](/Users/maywzh/Desktop/Screen Shot 2018-10-27 at 2.37.29 PM.png)

## 编写联合编译Makefile文件


再在根目录下编写一个联合编译的makefile，可以一步编译所有子目录的makefile      

联合编译Makefile文件：
```makefile
build:
    make -C ./dynamic
    #make -C:进入指定的目录下执行make命令
    make -C ./static
    #将静态、动态库文件拷贝到lib目录下
    cp ./static/libstatic_test.a  ./lib
	cp ./dynamic/libdynamic_test.so  ./lib

	make -C ./src

install:
	#为lib目录下的动态链接库文件在/usr/lib目录下创建软连接，这里使用的必须是绝对路径
	sudo ln -s /home/sunxiwang/03AgainStudyLinux/workspace/04makefile/03makefile/lib/libdynamic_test.so  /usr/lib/libdynamic_test.so 
	#将最后生成的可执行文件main拷贝到./bin目录下
	mv ./src/main ./bin

clean:
	make clean -C ./dynamic
	#make clean -C: 进入指定目录下执行make clean命令
	make clean -C ./static
	make clean -C ./src
```

执行

```bash
$ cd project
$ make build #构建
$ make install #生成可执行文件
$ make clean #清除编译文件
```

